## v6.0.0
* PIXI8 integration. Most of the interface is left the same. See /samples for generic usage samples.

## v5.1.0
* Library moved to TypeScript
* Dropped support of Canvas rendering (if you need it, please contact)

## v5.0.3
* Removed UMD package from "browser" script in package.json.

## v5.0.2
* By default ES6 script is used for the package instead of CommonJS

## v5.0.1
* pixi.js removed from direct dependencies.
* No more registerPlugins required before Application creating.

## v5.0.0
* Changed loading interface. Basically 'app.neutrino' changed to 'app.neutrino.loadData'.
* Plugins have to be registered through PIXINeutrino.registerPlugins(...).

## v4.0.0
* PIXI7 integration

## v2.3.3
* Fixed perspective projection for scaled effect.

## v2.3.2
* Respect world alpha of the container.

## v2.3.1
* Z axis of perspective projection swapped to look backward.

## v2.3.0
* Perspective projection.

## v2.2.1
* TAR archive on release page instead of plain .js.

## v2.2.0
* EffectModel interface changed to accept effect source and abstract TextureLoader.

## v2.1.3
* Own batch renderer for particles geometry.

## v2.1.2
* Syncronized to internal PIXI v5.2.0 changes _id -> _batchPosition

## v2.1.1
* Switched to neutrinoparticles.js@2.0.1

## v2.1.0
* Pause added to constructor of Effect.

## v2.0.0
* Sources ported to PIXI5.
* Tests, samples and documentation adjusted.

## v1.0.0
* PIXI4 rendering plugin.
* Samples.
* Basic Usage tutorial.

## v0.0.1
* Testing of release message.
