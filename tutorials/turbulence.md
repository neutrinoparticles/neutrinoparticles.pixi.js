# Using turbulence (or noise)

Turbulence and noise is the same feature. Turbulence force in Emitter Guide in the Editor implemented with Noise block in Emitter Scheme.

Before updating effects, which use noise, you need to initialize noise texture. Otherwise your effects will be updated without any noise.

You have two options: generate it or download it.

> You need to initialize noise only once per application.

## Generating noise texture

Right after creating `PIXI.Application` (or `PIXINeutrino.Context` if you don't use application class) you can call `generateNoise` for `PIXINeutrino.Context` object:

```javascript
app.neutrino.generateNoise();
```
It will generate noise in one call. It will block executing your code up to 2 seconds (dependently on running device).

Or you can spread generating process over many frames by using step-by-step generating function. Below is a sample which still generates everything in one loop, just to bring understanding how it works. It is up to you how to make these calls over different frames:

```javascript
let result;
do {
  result = app.neutrino.generateNoiseStep();
  const progress = result.progress; // Progress in range [0; 1]

  // do something with progress
  
} while (!result.done);
```

## Downloading noise texture

The NeutrinoParticles core library [neutrinoparticles.js](https://gitlab.com/neutrinoparticles/neutrinoparticles.js) provides a [way to download](https://gitlab.com/neutrinoparticles/neutrinoparticles.js#download-noise) the noise texture.

To make that using `PIXINeutrino.Context` you need to access underlying `Neutrino.Context` called `neutrino` as well:

```javascript
app.neutrino.neutrino.initializeNoise(
  "/path/to/noise/directory/",  // path to a directory where "neutrinoparticles.noise.bin" is located
  function() {},                // success callback
  function() {},                // fail callback
);
```

And, of course, you will need to distribute the texture file with your application.

## Source code
You can find source code related to this tutorial at [/samples/turbulence.html](../samples/turbulence.html).
