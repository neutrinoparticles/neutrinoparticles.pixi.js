# Basic Usage

## Getting access

First of all, you need to acquire a package object. Depending on your environment:

* HTML
```html
<script src="path/to/neutrinoparticles.pixi/dist/neutrinoparticles.pixi.umd.js"></script>
```
* node.js
```javascript
var PIXINeutrino = require('neutrinoparticles.pixi')
```
* ES6
```javascript
import * as PIXINeutrino from 'neutrinoparticles.pixi'
```

## Creating PIXI environment

You can create `PIXI.Application` or assemble environment manually by creating `PIXI.Renderer`, `PIXI.Loader` and all other necessary components.

To simplify this tutorial let's use `PIXI.Application`. It will create everything at once:

```javascript
const app = new PIXI.Application();

await app.init({
    width: 800,
    height: 600,
    neutrino: { // Additional options for PIXINeutrino.Context
      texturesBasePath: 'textures/'
    }
});
```

Note, that we passed options for creating `PIXINeutrino.Context` as `neutrino`. Here we specified `texturesBasePath`, which tells the context that all texture paths stored in effects have to be prefixed with `textures/`. And in case we have texture `backgrounds/dark01.png` in effect, `textures/backgrounds/dark01.png` will be requested for load.

`PIXINeutrino.Context` is accessible as `app.neutrino` for the code above.

> If you don't use `PIXI.Application` and create `PIXI.Renderer` manually, you will need to create `PIXINeutrino.Context` object manually as well.

And add rendering canvas to DOM:
```javascript
document.body.appendChild(app.canvas);
```

## Loading resources

You probably would prefer to load everything at once by a single load request:
```JavaScript
PIXI.Assets.addBundle('resources', [
	{ alias: 'background', src: 'textures/background.png' },
	{ alias: 'effectModel', src: 'export_js/water_stream.js', data: app.neutrino.loadData }
]);

PIXI.Assets.loadBundle('resources').then(onEverythingLoaded);
```
Here we requested for loading of texture `background.png` and effect `water_stream.js`.

Please note `data: app.neutrino.loadData` passed to load request. It shows to loader that the requested file is actually an effect and requires additional post-processing. After loading, it will be parsed and effect's model (`PIXINeutrino.EffectModel`) will be passed as the loaded resource.

## One model - many instances

In general, you load an effect only once, and add many instances of it on a scene.

`PIXINeutrino.EffectModel` describes behavior of effect and is not changed during application run.

`PIXINeutrino.Effect` represents an instance of effect on a scene. It has a state of effect which you can control at run-time. You may have many instances of the same effect's model.

## Making a scene

After resources are loaded, we need to fill a scene with objects to render:

```javascript
function onEverythingLoaded(resources) {
  // Create background sprite
  const back = new PIXI.Sprite(resources.background);
  app.stage.addChild(back);

  // Create effect
  let effect = new PIXINeutrino.Effect(resources.effectModel, {
    position: [400, 300, 0]
  });
  app.stage.addChild(effect);

  ...
```
Here we accessed to the effect's resource by `effectModel` key, which we passed to `PIXI.Assets.addBundle()`.

Additionaly you can pass options to effect like `position`, `rotation`, `scale` etc. (see API).

And the last thing to do to make it alive is to update the effect:

```javascript
  ...

  app.ticker.add((time) =>
  {
		const sec = time.deltaMS / 1000;

    effect.update(sec);
  });
};
```

We used PIXI's ticker to update effect each frame, but it's up to you how to organize update process.

## Source code
You can find source code for this tutorial at [/samples/basic_usage.html](../samples/basic_usage.html).
