# Scene hierarchy, rotation and scale

This tutorial touches different aspects of using effects in scene hierarchy. Also you will see how to correctly organize a scene to resize it without troubles.

## Scene graph

PIXI provides very convenient way to organize your objects in a hierarchy where one parent container has many children.

When parent container is moved, rotated or scaled, all its children are moved, rotated or scaled accordingly. And it looks like children are a solid part of the parent. So, all children live in parent coordinate system.

`PIXINeutrino.Effect` works in similar way. It can be attached to parent container, and it will respect parent's coordinate system to compute current position, rotation and scale.

The effect is a container by itself (as it inherited from `PIXI.Container`). This means, other objects can be attached to it.

## Particles are emitted in the global space

When you move your effect on the scene, you actually move only its current location. Consider it as a point where particles are emitted.

Particles are only created in position relative to that point. But after that, particles will not consider effect's position, they live in the global space.

> The global space can be overrided by `baseParent`. See below.

## Position, rotation and scale on construction

When you construct an effect, you can specify its position on the scene. There are `position`, `rotation` and `scale` options:

```javascript
let effect = new PIXINeutrino.Effect(resources.water_stream.effectModel, {
  position: [400, 300, 0],
  rotation: Math.PI / 4, // in radians
  scale: [2, 2, 2]
  // other options
});
```
All these options are defined in parent coordinate system. And if the effect is attached directly to the stage, these parameters can be considered as global.

In the sample above, if attached to the stage, the effect will be placed at position `{400, 300}` on the screen and will be twice bigger than original.

> Please note, that position and scale are 3-dimentional arrays, as effects always are simulated in 3D.

You can make non-uniform scale and stretch the effect only horizontally. In this case the scale will be `[2, 1, 1]`.

## Position, rotation and scale dynamically

After the effect is created all these parameters can be changed through its properties:
```javascript
effect.position.x = 200;
effect.position.y = 150;
// or
effect.position = new PIXI.Point(200, 150);

effect.positionZ = 0;

effect.rotation = Math.PI / 2;

effect.scale.x = 1;
effect.scale.y = 1;
// or
effect.scale = new PIXI.Point(1, 1);

effect.scaleZ = 1;
```
As you can see, for X and Y components standard `PIXI.Container` properties are used. But for Z component new properties `positionZ` and `scaleZ` introduced. 

Usually you don't need to use Z components at all.

## Scale dynamically carefully

Despite you can scale effects dynamically, you are strongly discouraged to do that.

At least don't scale when active particles are still on the screen. As you will see undesired shifting of that particles.

This restriction also covers scaling parent containers.

> The only parent container that can be correctly scaled is `baseParent`. See below.

## Rotation

To take into account the rotation specified for the effect, this effect has to be prepared correcly in the Editor before export. By default, it ignores rotation. It is made to reduce amount of computations. To change this behavior there are `Apply emitter's rotation` checkboxes in `Start postion` and `Changing position` sections of `Emitter Guide` in the Editor.

The rotation specified for the effect can be used for:
 * Rotating start position of particles - if enabled in `Start position` section.
 * Rotating starting velocity vector - if enabled in `Changing position -> Start velocity` section.

It will not affect already created particles in any way. 

What does that mean? 

Do you remember? They are emitted in the global space. 

For example, you have water stream effect (from `/samples/basic_usage.html`). In this effect, you have gravitation force directed downward, and you enabled `Apply emitter's rotation` for `Start velocity`. That means, the rotation will be applied only for direction of the stream at the moments when particles are created. And when they are shot, they become to live in global space where gravitation is constantly directed downward and they don't consider location or rotation of the effect as it doesn't matter any more.

> You can intentionally make the effect where particles somehow consider rotation of the effect during their lives, but it's more like expert tuning than standard behavior.

## Override the global space

There is a way to override the global space. You can specify one of parent containers of the effect as `baseParent`:
```javascript
let parent = new PIXI.Container();

let effect = new PIXINeutrino.Effect(resources.water_stream.effectModel, {
  position: [400, 300, 0],
  baseParent: parent
});

parent.addChild(effect);
```

or you can set it as a property after effect is created:
```javascript
effect.baseParent = parent;
```

This will define `parent` as a container for global space.

### What it gives?

When you move, rotate or scale `baseParent` container, it will move, rotate or scale the global space for all effects that have it as `baseParent`. Directly attached or through other child containers.

Moving global space means moving everything in effects, including already generated particles. It works like moving the screen when effects are playing.

### When it is useful?

It is a common case for game developers, when the application is designed for certain screen resolution (for example 1920x1080). There is a background drawn and other elements for such resolution. And you need to handle different sizes of canvas where the application is rendered.

The solution is pretty simple. To add a container to the stage (let's call it `main`) which will keep all our scene. And when we need to fit the scene into different canvas size, we scale and move this `main` container to keep the scene in the center of the canvas. With the same ratio as before and with a size to fit exactly in closest (horizontal or vertical) edges. 

In this case, all effects attached to `main` container have to use it as `baseParent`. It will keep the scene solid and resize effects on it instantly with all other sprites.

> Check `/samples/auto-resize-canvas-base-parent.html` sample to see how it works.

## Related samples
* [/samples/scale.html](../samples/scale.html) - Shows simple effects scaling.
* [/samples/autoresize-canvas.html](../samples/autoresize-canvas.html) - Shows resizing effects when browser window is resized. Without main container for the scene.
* [/samples/autoresize-canvas-base-parent.html](../samples/autoresize-canvas-base-parent.html) - Shows resizing effects when browser window is resized. With main container for the scene and background picture on it.


