# neutrinoparticles.pixi

The package allows you to render [NeutrinoParticles](https://neutrinoparticles.com/) effects in [PIXI.js](https://www.pixijs.com/) engine.

## PIXI versions
This package (`6.x.x`) works for **PIXI v8**

For other versions:
 * Versions `5.x.x` of this package are for PIXI v7. [go to branch](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/tree/5.x.x)
 * Versions `2.x.x` of this package are for PIXI v5 and PIXI v6. [go to branch](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/tree/2.x.x)
 * Versions `1.x.x` of this package are for PIXI v4. [go to branch](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/tree/1.x.x)
 

## API documentation

For more info about latest API you can check [API documentation](https://neutrinoparticles.gitlab.io/neutrinoparticles.pixi.js-doc/master/) pages.

For specific library version refer to [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/-/releases) page.

## Installation

You can install the package with `npm`:
```
> npm install neutrinoparticles.pixi
```
Or download pre-built package at [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/-/releases) page. There are [UMD](https://www.davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/), CommonJS and ES packages available (with TS typings).

## Quick usage

You can import effect into the application in two ways: statically, when built with an application; and dynamically, a source of an effect is downloaded via XHR request.

### ES6 + Static effects import

In the NeutrinoParticles Editor in the project settings you have to specify ES6 export:
![ES6 export in the project settings](./tutorials/img/proj_settings_es6.png)

And if you exported effect with name `test_effect`:

```javascript
import * as PIXI from 'pixi.js'
import * as PIXINeutrino from 'neutrinoparticles.pixi'

// Import simulation model. This effect has to be exported as (!) ES6 file.
import { NeutrinoEffect as TestEffect } from './export_js/test_effect.js'

// Create PIXI.Application
const app = new PIXI.Application();

await app.init({
  width: 800,
  height: 600,
  neutrino: {
    texturesBasePath: 'textures/' // Prefix for loading textures. Slash in the end!
  }
});

document.body.appendChild(app.canvas);

// Generate turbulence if necessary. Otherwise it won't work.
// app.neutrino.generateNoise();

// We need to specify how to load textures for effects. This will use PIXI.Assets.loader
const defaultTexturesLoader = new PIXINeutrino.DefaultAssetsTexturesLoader();

// Create an effect model. One model can be used to instantiate many effects on the scene.
const effectModel = new PIXINeutrino.EffectModel(

  // PIXINeutrino.Context. Added to the PIXI.Application by the plugin.
  app.neutrino, 

  // Creating simulation model from exported effect file.
  new TestEffect(app.neutrino.neutrino), 

  defaultTexturesLoader 
);

// Once all textures are loaded and the effect model is ready to use
effectModel.once('ready', () => {

  // Create instance of the effect and add it to the scene.
  const effect = new PIXINeutrino.Effect(effectModel, {
    position: [400, 300, 0], // Optional. There are also rotation, scale, paused etc.
  });
  app.stage.addChild(effect);

  // Call update for the effect on every frame.
  app.ticker.add((time) =>
  {
		const sec = time.deltaMS / 1000;

    effect.update(sec > 1.0 ? 1.0 : sec);
  });
});
```

### HTML + Dynamic effects loading
> Please note that in case of dynamic effects loading `eval()` is used!

In the NeutrinoParticles Editor in the project settings you have to specify `For XHR Request` export:

![ES6 export in the project settings](./tutorials/img/proj_settings_xhr.png)

And if you exported effect with name `test_effect`:

```html
...
<body>
<script src="/path/to/pixi.js/dist/pixi.js"></script>

<!-- Load both neutrinoparticles libraries -->
<script src="/path/to/neutrinoparticles.js/dist/neutrinoparticles.umd.js"></script>
<script src="/path/to/neutrinoparticles.pixi.umd.js"></script>

<script>

// Create PIXI.Application
const app = new PIXI.Application();

await app.init({
  width: 800,
  height: 600,
  neutrino: {
    texturesBasePath: 'textures/' // Prefix for loading textures. Slash in the end!
  }
});

document.body.appendChild(app.canvas);

// Generate turbulence if necessary. Otherwise it won't work.
// app.neutrino.generateNoise();

// Create bundle for loading resources
PIXI.Assets.addBundle('resources', [
  { 
    alias: 'testEffect', // Resource name for further access
    src: 'export_js/test_effect.js', // Path to an effect exported (!) for XHR Request
    data: app.neutrino.loadData // Mandatory. Indicates NeutrinoParticles effect.
  }
]);

// Load the bundle
const resources = await PIXI.Assets.loadBundle('resources');

// Create instance of the effect and add it to the scene.
const effect = new PIXINeutrino.Effect(resources.testEffect, {
  position: [400, 300, 0], // Optional. There are also rotation, scale, paused etc.
});
app.stage.addChild(effect);

// Call update for the effect on every frame.
app.ticker.add((time) =>
{
	const sec = time.deltaMS / 1000;

  effect.update(sec > 1.0 ? 1.0 : sec);
});

</script>
</body>
```

## Tutorials
* [Basic Usage](./tutorials/basic_usage.md)
* [Using turbulence (or noise)](./tutorials/turbulence.md)
* [Scene hierarchy, rotation and scale](./tutorials/scene_hierarchy_rotation_and_scale.md)

## Samples
There are several samples in folder `/samples` which you can refer to.

#### Running samples
To run the samples you will need web server running on localhost:80 with a web root on the root repository folder. If you have Python3 installed you can use `/start_http_server.sh` script to start it.

After server is running you can access the samples by `http://localhost/samples/<sample_name>.html`
> For different web server configuration, please adjust the path above accordingly.

#### Debug Samples with VSCode

You can use following launch configuration to start debuging of samples:
```json
{
    "name": "Samples in Chrome",
    "type": "chrome",
    "runtimeExecutable": "/path/to/chrome",
    "runtimeArgs": [
        "--incognito",
        "--new-window",
        "--remote-debugging-port=9222",
        "--user-data-dir=remote-profile"
    ],
    "request": "launch",
    "url": "http://localhost/samples/simplest.html",
    "webRoot": "${workspaceFolder}",
    "breakOnLoad": true,
    "sourceMaps": true,
    "port": 9222,
}
```
> To be able to set breakpoint in .html files you will need to enable `Allow Breakpoints Everywhere` flag in VSCode Settings.

## Tests
#### Running tests

To run tests you can use `npm run test` command as usual. It will start IBCT (image based comparison tests). You will need to provide GPU environment to run the tests. It can be hardware video card or software renderer.

#### Debug with VSCode

You can use following launch configuration to start debugging of main thread of electron-mocha application which perform the tests:

```json
{
	"name": "Debug Tests",
	"type": "node",
	"request": "launch",
	"cwd": "${workspaceRoot}",
	"runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron-mocha",
	"windows": {
	  "runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron-mocha.cmd"
	},
	"program": "${workspaceRoot}/test/test.js",
	"args": [
		"--require", "@babel/register",
		"--timeout", "999999",
		"--colors",
		"--ibct-expect-dir", "__my_expect__",
		"test/test.js"
	],
	"console": "integratedTerminal",
	"protocol": "inspector",
}
```
> To be able to set breakpoint in .html files you will need to enable `Allow Breakpoints Everywhere` flag in VSCode Settings.

Please note, that `ibct-expect-dir` argument overrides default expectation files directory `__expect__` for IBCT. The directory is placed in /test/ibct. Such behaviour is useful when running tests locally, because different hardware gives different results and default expectations will most probably fail for you.

## Contribution
We will be much appreciated for any fix/suggestion/feature merge requests and will consider them ASAP.

There are not much rules for such requests, just be sure that all tests are passing.
