// @ts-ignore
import * as Neutrino from 'neutrinoparticles.js';
import { Renderer } from 'pixi.js';
import { NeutrinoContext, NeutrinoNoiseGenerator } from './NeutrinoParticlesTypes';

export interface ContextOptions {
    texturesBasePath?: string,
    trimmedExtensionsLookupFirst?: boolean
}

export interface ContextLoadData {
    __neutrinoContext__: Context;
}

export interface ContextNoiseGenProgress {
    done: boolean;
    progress: number;
}

/**
 * Main NeutrinoParticles context for PIXI.
 * 
 * It has to be created before loading or using any effects in the application.
 * 
 * Usually, the context is created automatically by {@link ApplicationPlugin} after
 * PIXI.Application is created and assigned to PIXI.Application.neutrino property.
 * 
 * @param {PIXI.Renderer} renderer Current PIXI renderer.
 * @param {PIXI.Loader} loader A loader which will be used to load textures and effects.
 * @param {Object} [options={}] Options
 * @param {string} [options.texturesBasePath=""] This path will be added before any texture
 * path stored in a loaded effect.
 * @param {boolean} [options.trimmedExtensionsLookupFirst=true] Before loading any texture
 * during loading effect, will check if there is already loaded texture in the cache
 * with the same name but without extension. It is useful for atlases, as they have
 * sometimes textures without extensions in their description.
 */

export class Context {
    /**
     * The context of 
     * {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js} library.
     */
    public readonly neutrino: NeutrinoContext;
    public readonly options: ContextOptions;
    public readonly loadData: ContextLoadData;

    private _noiseInitialized = false;
    /**
     * In case if noise is being generated in multiple steps by generateNoiseStep(),
     * this will store NeutrinoParticles.NoiseGenerator until the process is finished.
     * @private
     */
    private _noiseGenerator?: NeutrinoNoiseGenerator;

	constructor(_renderer: Renderer, options: ContextOptions) {
		
		this.options = Object.assign({
            texturesBasePath: "",
            trimmedExtensionsLookupFirst: true
        }, options);
        
		this.neutrino = new Neutrino.Context();

        this.loadData = {
            __neutrinoContext__: this
        };
	}

    /**
     * Initiates loading of pre-generated noise file. This file you can find
     * in repository of 
     * {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js} 
     * library. You will need to deploy this file with your application.
     * 
     * @param {string} path Path to the directory where 'neutrinoparticles.noise.bin' file
     * is located.
     * @param {function()} success Success callback.
     * @param {function()} fail Fail callback.
     */
	public initializeNoise(path: string, success: () => void, fail: () => void)
    {
        if (this._noiseInitialized)
        {
            if (success) success();
            return;
        }

        this.neutrino.initializeNoise(path, 
            () => { 
                this._noiseInitialized = true; 
                if (success) success(); 
            }, 
            fail);
    }

    /**
     * Generates noise for effects in one call. It will lock script executing until finished.
     * 
     * This method should be called only once in the start of the application.
     */
	public generateNoise()
    {
        if (this._noiseInitialized)
            return;

        let noiseGenerator = new this.neutrino.NoiseGenerator();
        while (!noiseGenerator.step()) { // approx. 5,000 steps
            // you can use 'noiseGenerator.progress' to get generating progress from 0.0 to 1.0
        }

        this._noiseInitialized = true;
    }
    
    /**
     * Generates noise in multiple steps. With this function you can spread noise
     * generation through multiple frames.
     * 
     * Generating noise should be made only once in the start of the application.
     * 
     * @example
     * let result;
     * do {
     *   result = app.neutrino.generateNoiseStep();
     *   const progress = result.progress; // Progress in range [0; 1]
     * 
     *   // do something with progress
     *   
     * } while (!result.done);
     */
    public generateNoiseStep(): ContextNoiseGenProgress
    {
        if (this._noiseInitialized)
        {
            return { done: true, progress: 1.0 };
        }

        if (!this._noiseGenerator)
            this._noiseGenerator = new this.neutrino.NoiseGenerator();

        const done = this._noiseGenerator.step();
        const progress = this._noiseGenerator.progress;

        if (done)
        {
            this._noiseInitialized = true;
            this._noiseGenerator = undefined;
        }

        return { done, progress };
    }
}
