import { OutputQuad, QuadBuffer } from './QuadBuffer';
import { EffectModel } from './EffectModel';
import { Pause, Vec2, Vec3 } from './types';
import { Bounds, Container, Instruction, Point, Renderer, ViewContainer } from 'pixi.js';
import { PerspectiveProjection } from './PerspectiveProjection';
import { Context } from './Context';
import { NeutrinoEffect } from './NeutrinoParticlesTypes';
import { EffectPipe } from './EffectPipe';

export interface EffectOptions {
	position?: Vec3,
	rotation?: number,
	scale?: Vec3,
	pause?: Pause,
	generatorsPaused?: boolean,
	baseParent?: Container,
	projection?: PerspectiveProjection,
	autoInit?: boolean
}

interface EffectStartupOptions {
	paused: boolean,
	generatorsPaused: boolean
}

const emptyBounds = new Bounds(0, 0, 0, 0);

/**
 * Represents an instance of effect on the scene.
 * 
 * The class is a wrapper around exported NeutrinoParticles effect. It
 * introduces everything to place effect on a PIXI scene.
 * 
 * @example
 * Assets.addBundle('resources', [
 *   // Add effect as a part of a bundle. And don't forget to specify context in 'data' property
 *   { name: 'effectModel', srcs: 'export_js/noise.js', data: app.neutrino.loadData }
 * ]);
 * 
 * Assets.loadBundle('resources').then((resources) => {
 *   // Create effect instance using loaded model
 *   let effect = new PIXINeutrino.Effect(resources.effectModel, {
 *     position: [400, 300, 0]
 *   });
 *   // Add effect on the scene
 *   app.stage.addChild(effect);
 * 	 // Update effect with PIXI's ticker
 *   app.ticker.add((delta) => {
 * 	   const msec = delta / settings.TARGET_FPMS;
 *     const sec = msec / 1000.0;
 * 	   effect.update(sec);
 *   });
 * });
 * 
 * @param {EffectModel} effectModel A model which is used to instantiate and simulate effect.
 * @param {Object} [options={}] Options
 * @param {Array} [options.position=[0, 0, 0]] Location of the effect on a scene.
 * @param {number} [options.rotation=0] Rotation of the effect in radians. 
 * @param {Array} [options.scale=[1, 1, 1]]  Scale by axes for the effect.
 * @param {Pause} [options.pause=Pause.BEFORE_UPDATE_OR_RENDER] Pause type on start.
 * @param {boolean} [options.generatorsPaused=false] Generators paused on start.
 * @param {Container} [options.baseParent=null] An object which defines global coordinate
 *   system for the effect. By default, identity coordinate system used.
 * @param {PerspectiveProjection} [options.projection=null] Projection to use in the effect. If null - the
 *   effect will be rendered using orthogonal projection (by using only X and Y values of vertices).
 * @param {boolean} [options.autoInit=true] Automatically initialize the effect. If false, you have to call
 *   {@link Effect#init} manually. Useful for effects with presimulation to postpone initialization)
 *   until all scene hierarchy is set up.
 */
export class Effect extends ViewContainer implements Instruction {

	public override readonly renderPipeId: string = 'neutrino';
	public override batched: boolean = false;

	/** 
	 * {@link Context} used to create the effect. 
	 */
	public readonly ctx: Context;

	/** 
	 * {@link EffectModel} used to create the effect. 
	 */
	public readonly effectModel: EffectModel;

	/** 
	 * Underlying {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js}
	 * effect object. 
	 */
	public effect?: NeutrinoEffect;

	/**
	 * Container which is used as global coordinate system provider.
	 * It must be one of parent containers of the effect.
	 */
	public readonly baseParent?: Container;

	/** 
	 * Z coordinate of position. X and Y coordinates are used from Container.position. 
	 */
	public positionZ: number;

	/** 
	 * Z coordinate of scale vector. X and Y coordinates are used from Container.scale. 
	 */
	public scaleZ: number;

	private _worldPosition: Point = new Point(0, 0);
	private _worldRotationDegree: number = 0;
	private _worldScale: Point = new Point(1, 1);
	private _projection?: PerspectiveProjection;
	private _unpauseOnUpdateRender: boolean;
	private _startupOptions: EffectStartupOptions;
	private _inited: boolean = false;
	private _ready: boolean = false;
	private _renderer?: Renderer;
	private _renderBuffers?: QuadBuffer;

	constructor(effectModel: EffectModel, options: EffectOptions) {
		super({});

		options = Object.assign({
			position: [0, 0, 0],
			rotation: 0,
			scale: [1, 1, 1],
			pause: Pause.BEFORE_UPDATE_OR_RENDER,
			generatorsPaused: false,
			baseParent: null,
			projection: null,
			autoInit: true
		}, options);

		
		this.ctx = effectModel.ctx;

		this.effectModel = effectModel;
		
		this.baseParent = options.baseParent;

		this.position.set(options.position![0], options.position![1]);

		this.positionZ = options.position![2];

		this.rotation = options.rotation!;

		this.scale.x = options.scale![0];
		this.scale.y = options.scale![1];
		this.scaleZ = options.scale![2];

		this._projection = options.projection;

		this._startupOptions = {
			paused: options.pause !== Pause.NO,
			generatorsPaused: options.generatorsPaused!
		};
		this._unpauseOnUpdateRender = (options.pause === Pause.BEFORE_UPDATE_OR_RENDER);

		if (options.autoInit)
			this.init();
	}

	public get bounds()
    {
        return emptyBounds;
    }

    protected override updateBounds(): void {}

	/**
	 * Initializes the effect. Usually called automatically from the constructor if 'autoInit'
	 * option is set to true during effect creation.
	 */
	init(): void {
		if (this._inited) return;

		this._inited = true;

		if (this.effectModel.ready()) {
			this._onEffectReady();
		} else {
			this.effectModel.once('ready', () => {
				this._onEffectReady();
			}, this);
		}
	}

	/**
	 * Returns true if the effect is ready to update/render. 
	 * 
	 * Basically that means that {@link Effect#effectModel} is loaded 
	 * and all textures required to render this effect are also loaded.
	 * 
	 * @returns {boolean} True if the effect can be rendered.
	 */
	ready(): boolean {
		return this._ready;
	}

	/**
	 * Simulates effect for specified time.
	 * 
	 * @example
	 * let effect = new PIXINeutrino.Effect(resources.some_effect.effectModel);
	 * app.ticker.add((delta) => {
	 * 	 const msec = delta / settings.TARGET_FPMS;
	 *   const sec = msec / 1000.0;
	 *   effect.update(sec);
	 * });
	 * 
	 * @param {number} seconds Time in seconds.
	 */
	update(seconds: number): void {
		if (!this.ready())
			return;

		this._checkUnpauseOnUpdateRender();

		this._updateWorldTransform();
		
		if (this.effect != null) {
			this.effect.update(seconds, this._scaledPosition(),
				this.ctx.neutrino.axisangle2quat_([0, 0, 1], this._worldRotationDegree));

			this.onViewUpdate();
		}
	}

	/**
	 * Restarts the effect.
	 * 
	 * @param {Array} position A new position of the effect or null to keep the same position.
	 * @param {number} rotation A new rotation of the effect of null to keep the same rotation.
	 */
	restart(position?: Vec3, rotation?: number): void {
		if (position) {
			this.position.x = position[0];
			this.position.y = position[1];
			this.positionZ = position[2];
		}

		if (rotation) {
			this.rotation = rotation;
		}

		this._updateWorldTransform();

		this.effect!.restart(this._scaledPosition(),
			rotation ? this.ctx.neutrino.axisangle2quat_([0, 0, 1], this._worldRotationDegree) 
			: undefined);

		this.onViewUpdate();
	}

	/**
	 * Instant change of position/rotation of the effect. 
	 * 
	 * While {@link Effect#update} changes position 
	 * of the effect smoothly interpolated, this function will prevent such smooth trail
	 * from previous position and "teleport" effect instantly.
	 * 
	 * @param {Array} position A new position of the effect or null to keep the same position.
	 * @param {number} rotation A new rotation of the effect of null to keep the same rotation.
	 */
	resetPosition(position?: Vec3, rotation?: number): void {
		if (position) {
			this.position.x = position[0];
			this.position.y = position[1];
			this.positionZ = position[2];
		}

		if (rotation) {
			this.rotation = rotation;
		}

		this._updateWorldTransform();

		this.effect!.resetPosition(this._scaledPosition(),
			rotation ? this.ctx.neutrino.axisangle2quat_([0, 0, 1], this._worldRotationDegree) : undefined);

		this.onViewUpdate();
	}

	/** Pauses the effect. All active particles will freeze. */
	pause(): void {
		if (this.ready())
			this.effect!.pauseAllEmitters();
		else
			this._startupOptions.paused = true;
	}

	/** Unpauses the effect. */
	unpause(): void {
		if (this.ready())
			this.effect!.unpauseAllEmitters();
		else
			this._startupOptions.paused = false;
	}

	/** 
	 * Pauses all generators in the effect. No new particles will be generated.
	 * All current particles will continue to update.
	 */
	pauseGenerators(): void {
		if (this.ready())
			this.effect!.pauseGeneratorsInAllEmitters();
		else
			this._startupOptions.generatorsPaused = true;
	}

	/** Unpauses generators in the effect. */
	unpauseGenerators(): void {
		if (this.ready())
			this.effect!.unpauseGeneratorsInAllEmitters();
		else
			this._startupOptions.generatorsPaused = false;
	}

	/**
	 * Sets a value of emitter property in all emitters of the effect.
	 * 
	 * @param {string} name Name of property.
	 * @param {number|Array} value Value of property. It can be a Number, 
	 * 2-, 3- or 4-component Array depending on a property you want to set.
	 */
	setPropertyInAllEmitters(name: string, value: Vec3 | Vec2 | number): void {
		this.effect!.setPropertyInAllEmitters(name, value);
		this.onViewUpdate();
	}

	/**
	 * @returns {number} A number of active particles in the effect.
	 */
	getNumParticles(): number {
		return this.effect!.getNumParticles();
	}

	_generateGeometry(renderer: Renderer) {
        if (!this.ready())
			return;

		this._checkUnpauseOnUpdateRender();

		this._renderer = renderer;

		if (this._projection) {
			this._projection.setScreenFrame && this._projection.setScreenFrame(renderer.screen);
		}

		//renderer.batch.setObjectRenderer(renderer.plugins.neutrino);

		const wt = this._renderBuffers!.worldTransform;

		if (this.baseParent)
			{
				const sx = this._worldScale.x;
				const sy = this._worldScale.y;
				const m = this.baseParent.worldTransform;
				wt[0] = m.a * sx;
				wt[1] = m.b * sy;
				wt[2] = m.c * sx;
				wt[3] = m.d * sy;
				wt[4] = m.tx * sx;
				wt[5] = m.ty * sy;
			} else {
				wt[0] = this._worldScale.x;
				wt[1] = 0;
				wt[2] = 0;
				wt[3] = this._worldScale.y;
				wt[4] = 0;
				wt[5] = 0;
			}

		this._renderBuffers!.worldAlpha = this.localAlpha * 
			(this.parentRenderGroup ? this.parentRenderGroup.worldAlpha : 1.0);

		this.effect!.fillGeometryBuffers([1, 0, 0], [0, -1, 0], [0, 0, -1]);

		this._renderer = undefined;
    }

	private _scaledPosition(): Vec3 {
		return [this._worldPosition.x / this._worldScale.x, this._worldPosition.y / 
			this._worldScale.y, this.positionZ / this.scaleZ];
	}

	private _getRenderGroupContainer(container: Container): Container {
		if (container.renderGroup || !container.parent)
			return container;

		return this._getRenderGroupContainer(container.parent);
	}

	private _updateWorldTransform() {
		var localPosition = new Point(0, 0);
		var localXAxis = new Point(1, 0);
		var localYAxis = new Point(0, 1);

		var worldXAxis, worldYAxis;

		const baseParent = this.baseParent || this._getRenderGroupContainer(this);

		this._worldPosition = baseParent.toLocal(localPosition, this);
		worldXAxis = baseParent.toLocal(localXAxis, this);
		worldYAxis = baseParent.toLocal(localYAxis, this);

		worldXAxis.x -= this._worldPosition.x;
		worldXAxis.y -= this._worldPosition.y;
		worldYAxis.x -= this._worldPosition.x;
		worldYAxis.y -= this._worldPosition.y;

		this._worldScale = new Point(
			Math.sqrt(worldXAxis.x * worldXAxis.x + worldXAxis.y * worldXAxis.y),
			Math.sqrt(worldYAxis.x * worldYAxis.x + worldYAxis.y * worldYAxis.y)
		);

		this._worldRotationDegree = (this._calcWorldRotation(this) / Math.PI * 180) % 360;
	}

	private _calcWorldRotation(obj: Container): number {
		if (obj.parent && obj.parent != this.baseParent)
			return obj.rotation + this._calcWorldRotation(obj.parent);
		else
			return obj.rotation;
	}

	private _onEffectReady(): void {
		this._renderBuffers = new QuadBuffer(
			(vertices: OutputQuad, renderStyle: number) => {
				this._pushQuad(vertices, renderStyle);
			},
			this._projection);

		this._updateWorldTransform();
		this.effect = this.effectModel.effectModel.createWGLInstance(this._scaledPosition(),
			this.ctx.neutrino.axisangle2quat_([0, 0, 1], this._worldRotationDegree), this._renderBuffers,
			this._startupOptions);
		this.effect.texturesRemap = this.effectModel.texturesRemap;

		this._ready = true;
		this.emit('ready', this);
	}

	private _checkUnpauseOnUpdateRender() {
		if (this._unpauseOnUpdateRender) {
			this.resetPosition();
			this.unpause();
			this._unpauseOnUpdateRender = false;
		}
	}

	private _pushQuad(_vertices: OutputQuad, renderStyleIndex: number): void
	{
		const renderStyle = this.effect!.model.renderStyles[renderStyleIndex];
		const texIndex = renderStyle.textureIndices[0];
		const texture = this.effectModel.textures[texIndex];
		const blendMode = this.effectModel.blendModes[renderStyleIndex];

		const effectPipe = (this._renderer!.renderPipes as any)['neutrino'] as EffectPipe;
		effectPipe.batchQuad(_vertices, texture, blendMode);
	}
}
