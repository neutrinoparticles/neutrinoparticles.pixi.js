import { BLEND_MODES, Cache, EventEmitter, getAdjustedBlendModeBlend, Texture } from "pixi.js";
import { Context } from "./Context";
import { AbstractTexturesLoader } from "./TexturesLoader";
import { NeutrinoEffectModel, NeutrinoSubRect } from "./NeutrinoParticlesTypes";

/**
 * Represents an effect model.
 * 
 * It actually wraps {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js}
 * exported effect model to perform all necessary PIXI-related integration.
 * 
 * Used in {@link Effect} constructor.
 * 
 * @param {Context} context Main context for effects.
 * @param {string|object} neutrinoModel A string with source code of loaded effect, or created effect object.
 * @param {AbstractTexturesLoader} texturesLoader Textures loader. This object will be used only if textures used
 * by the effect are not loaded yet.
 */
export class EffectModel extends EventEmitter
{
	public readonly ctx: Context;
	public readonly effectModel: NeutrinoEffectModel;

	public texturesRemap: Array<NeutrinoSubRect | null> = [];
	public readonly textures: Array<Texture> = [];
	public readonly blendModes: Array<BLEND_MODES> = [];

	private _numTexturesToLoadLeft: number;

	constructor(context: Context, neutrinoModel: string | NeutrinoEffectModel, texturesLoader: AbstractTexturesLoader) 
	{
		super();

		this.ctx = context;

		if (typeof neutrinoModel === "string") {
			let evalScript = "(function(ctx) {\n" + neutrinoModel + 
					"\nreturn new NeutrinoEffect(ctx);\n})(context.neutrino);";
			this.effectModel = eval(evalScript);
		} else {
			this.effectModel = neutrinoModel;
		}

		let numTextures = this.effectModel.textures.length;
		this._numTexturesToLoadLeft = numTextures;

		for (let texIndex = 0; texIndex < numTextures; ++texIndex) 
		{
			let texturePath = this.effectModel.textures[texIndex];
			let texture = null;
			
			if (this.ctx.options.trimmedExtensionsLookupFirst) 
			{
				let trimmedTexturePath = texturePath.replace(/\.[^/.]+$/, ""); // https://stackoverflow.com/a/4250408
				
				if (Cache.has(trimmedTexturePath))
					texture = Cache.get(trimmedTexturePath);
			}

			if (!texture && Cache.has(texturePath))
				texture = Cache.get(texturePath);
			
			if (texture)
			{
				this._onTextureLoaded(texIndex, texture);
			}
			else
			{
				texturesLoader.load(this.ctx.options.texturesBasePath + texturePath).then(
					((self, imageIndex: number) => {
						return function(texture) 
						{
							self._onTextureLoaded(imageIndex, texture);
						}
					}) (this, texIndex));
			}			
		}
	}

	/**
	 * @returns true, if all related resources are loaded and the model is ready to use.
	 */
	public ready() 
	{
		return this._numTexturesToLoadLeft === 0;
	}

	private _onTextureLoaded(index: number, texture: Texture) 
	{
		this.textures[index] = texture;

		this._numTexturesToLoadLeft--;

		if (this._numTexturesToLoadLeft === 0) 
		{
			this._initTexturesRemapIfNeeded();
			this._initBlendModes();
			this.emit('ready', this);
		}
	}

	private _initTexturesRemapIfNeeded() 
	{
		let remapNeeded = false;

		for (let texIdx = 0; texIdx < this.textures.length; ++texIdx) 
		{
			let texture = this.textures[texIdx];

			if (texture.frame.x != 0 || texture.frame.y != 0
				|| texture.frame.width != texture.source.pixelWidth
				|| texture.frame.height != texture.source.pixelHeight) 
			{
				remapNeeded = true;
				break;
			}
		}

		if (!remapNeeded) 
			return;

		for (let texIdx = 0; texIdx < this.textures.length; ++texIdx) 
		{
			let texture = this.textures[texIdx];

			this.texturesRemap[texIdx] = new this.ctx.neutrino.SubRect(
				texture.frame.x / texture.source.pixelWidth,
				1.0 - (texture.frame.y + texture.frame.height) / texture.source.pixelHeight,
				texture.frame.width / texture.source.pixelWidth,
				texture.frame.height / texture.source.pixelHeight
				);
		}
	}

	private _initBlendModes()
	{
		const numRenderStyles = this.effectModel.renderStyles.length;

		for (let renderStyleIndex = 0; renderStyleIndex < numRenderStyles; ++renderStyleIndex)
		{
			const renderStyle = this.effectModel.renderStyles[renderStyleIndex];
			const texIndex = renderStyle.textureIndices[0];
			const texture = this.textures[texIndex];
			const material = this.effectModel.materials[renderStyle.materialIndex];

			let blendMode: BLEND_MODES;
			switch (material) 
			{
				default: blendMode = getAdjustedBlendModeBlend('normal', texture.source); break;
				case 1: blendMode = getAdjustedBlendModeBlend('add', texture.source); break;
				case 2: blendMode = getAdjustedBlendModeBlend('multiply', texture.source); break;
			}

			this.blendModes.push(blendMode);
		}
	}
}