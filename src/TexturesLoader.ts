import { Assets, Loader, Texture } from 'pixi.js'

/**
 * Texture loader interface used by {@link EffectModel} to load necessary textures.
 */
export abstract class AbstractTexturesLoader
{
    constructor() {
    }

    /**
	 * Requests loading of a texture.
	 * 
	 * @param {string} texturePath Path to texture.
	 */
    abstract load(texturePath: string): Promise<Texture>;
}

export class AssetsTexturesLoader extends AbstractTexturesLoader
{
	private loader: Loader;

	constructor(loader: Loader) {
		super();

		this.loader = loader;
	}
	
	async load(texturePath: string): Promise<Texture> {
		return await this.loader.load(texturePath);
	}

};

export class DefaultAssetsTexturesLoader extends AssetsTexturesLoader
{
	public static readonly instance: DefaultAssetsTexturesLoader = new DefaultAssetsTexturesLoader();

	private constructor() {
		super(Assets.loader);
	}
};
