import { ApplicationPlugin } from './AplicationPlugin';
import { Context, ContextOptions } from './Context';
import { effectModelLoader } from './EffectModelLoader';
import { extensions } from 'pixi.js';
import { EffectPipe } from './EffectPipe';
import { EffectBatcher } from './EffectBatcher';

export * from './types';
export * from './AplicationPlugin';
export * from './Context';
export * from './Effect';
export * from './EffectModel';
export * from './QuadBuffer';
export * from './TexturesLoader';
export * from './PerspectiveProjection';
export * from './EffectModelLoader';
export * from './NeutrinoParticlesTypes';
export * from './EffectPipe';
export * from './EffectBatcher';

export function registerPlugins()
{
    extensions.add(ApplicationPlugin);
    extensions.add(effectModelLoader);
    extensions.add(EffectPipe);
    extensions.add(EffectBatcher);
}

export function unregisterPlugins()
{
    extensions.remove(ApplicationPlugin);
    extensions.remove(effectModelLoader);
    extensions.remove(EffectPipe);
    extensions.remove(EffectBatcher);
}

declare module 'pixi.js' {
    interface IApplicationOptions {
        neutrino?: ContextOptions;
    }

    interface Application {
        neutrino?: Context;
    }

    interface BaseTexture {
        _neutrinoTick?: number;
        _neutrinoBatchLocation?: number;
    }
}

registerPlugins()