import { 
    Batch
    , BatchableElement
    , BatchAction
    , Batcher
    , BatchGeometry
    , BLEND_MODES, DefaultShader
    , ExtensionType
    , fastCopy
    , Geometry
    , getMaxTexturesPerBatch
    , IndexBufferArray
    , InstructionSet
    , Shader
    , Texture
    , Topology, uid
    , ViewableBuffer
} from "pixi.js";
import { OutputQuad } from "./QuadBuffer";

const batchPool: Batch[] = [];
let batchPoolIndex = 0;

function getBatchFromPool()
{
    return batchPoolIndex > 0 ? batchPool[--batchPoolIndex] : new Batch();
}

function returnBatchToPool(batch: Batch)
{
    batchPool[batchPoolIndex++] = batch;
}

export class BatchableParticleElement implements BatchableElement
{
    // Constant
    batcherName: string = EffectBatcher.extension.name;
    indexSize: number = 6;
    attributeSize: number = 6 * 4;
    topology: Topology = 'triangle-list';
    packAsQuad: boolean = false;

    // Dynamically filled
    texture: Texture = null as any as Texture;
    blendMode: BLEND_MODES = 'normal';
    quad: OutputQuad = null as any as OutputQuad;
    instructionSet: InstructionSet = null as any as InstructionSet;

    // Always null
    _textureId: number = null as any as number;
    _attributeStart: number = null as any as number;
    _indexStart: number = null as any as number;
    _batcher: Batcher = null as any as Batcher;
    _batch: Batch = null as any as Batch;
}

let BATCH_TICK = 0;

export interface EffectBatcherOptions
{
    maxTextures?: number;
    initialNumParticles?: number;
}

export class EffectBatcher
{
    public static defaultOptions: Partial<EffectBatcherOptions> = {
        maxTextures: undefined, // Will take maximum number of textures supported by the device
        initialNumParticles: 256,
    };

    /** @ignore */
    public static extension = {
        type: [
            ExtensionType.Batcher,
        ],
        name: 'neutrino',
    } as const;

    public readonly uid: number = uid('batcher');

    public attributeBuffer: ViewableBuffer;

    public indexBuffer: IndexBufferArray;

    public attributeSize: number = 0;

    public indexSize: number = 0;

    public dirty = true;

    public geometry: Geometry = new BatchGeometry();

    public shader: Shader;

    public batches: Batch[] = [];

    public readonly name: string = EffectBatcher.extension.name;
    
    private _numBatches = 0;
    private _batchIndexStart: number = 0;
    private _batchIndexSize: number = 0;
    private readonly _maxTextures: number;
    private _action: BatchAction = 'startBatch';

    constructor(options: EffectBatcherOptions = {})
    {
        options = { ...EffectBatcher.defaultOptions, ...options };

        // numFloatsPerVertex * numVertices * numBytesPerFloat
        this.attributeBuffer = new ViewableBuffer(options.initialNumParticles! * 6 * 4 * 4); 

        this.indexBuffer = new Uint16Array();
        this._maxTextures = options.maxTextures ?? getMaxTexturesPerBatch();
        this.shader = new DefaultShader(this._maxTextures)

        // Resize and fill with indices
        this._resizeIndexBuffer(options.initialNumParticles! * 6);
    }

    public begin()
    {
        this.indexSize = 0;
        this.attributeSize = 0;

        for (let i = 0; i < this._numBatches; i++)
        {
            returnBatchToPool(this.batches[i]);
            this.batches[i] = null as any as Batch;
        }

        this._numBatches = 0;
        this._batchIndexStart = 0;
        this._batchIndexSize = 0;

        this.dirty = true;
        this._action = 'startBatch';
    }

    public add(batchableObject: BatchableElement)
    {
        const particleElement = (batchableObject as BatchableParticleElement);

        if (this._batchIndexSize === this._batchIndexStart) {
            this._pushNewBatch(particleElement.blendMode);
        }

        let batch = this.batches[this._numBatches - 1];
        let textureBatch = batch.textures;
        let blendMode = batch.blendMode;

        const newAttributeSize = this.attributeSize + batchableObject.attributeSize;
        const newAttributeSizeInBytes = newAttributeSize * 4;
        if (newAttributeSizeInBytes > this.attributeBuffer.size)
        {
            this._resizeAttributeBuffer(newAttributeSizeInBytes);
        }

        const newIndexSize = this.indexSize + batchableObject.indexSize;
        if (newIndexSize > this.indexBuffer.length)
        {
            this._resizeIndexBuffer(newIndexSize);
        }

        const textureSource = particleElement.texture._source;
        const breakRequired = blendMode !== particleElement.blendMode;

        if (textureSource._batchTick === BATCH_TICK && !breakRequired)
        {
            this._batchIndexSize += particleElement.indexSize;
            this._packAttributes(particleElement, textureSource._textureBindLocation);
        }
        else
        {
            if (textureBatch.count >= this._maxTextures || breakRequired)
            {
                this._finishBatch(particleElement.instructionSet);

                this._action = 'renderBatch';
                
                // create a batch...
                batch = this._pushNewBatch(particleElement.blendMode);
                textureBatch = batch.textures;
            }

            textureSource._batchTick = BATCH_TICK;

            textureSource._textureBindLocation = textureBatch.count;
            textureBatch.ids[textureSource.uid] = textureBatch.count;
            textureBatch.textures[textureBatch.count++] = textureSource;

            this._batchIndexSize += particleElement.indexSize;

            this._packAttributes(particleElement, textureSource._textureBindLocation);
        }

        this.indexSize = newIndexSize;
        this.attributeSize = newAttributeSize;
    }

    public checkAndUpdateTexture(_batchableObject: BatchableElement, _texture: Texture): boolean
    {
        // Not used
        return true;
    }

    public updateElement(_batchableObject: BatchableElement)
    {
        // Not used
    }

    public break(instructionSet: InstructionSet)
    {
        if (this._batchIndexStart === this._batchIndexSize) return;

        this._finishBatch(instructionSet);

        this._action = 'startBatch';
    }

    private _pushNewBatch(blendMode: BLEND_MODES): Batch
    {
        const batch = getBatchFromPool();
        batch.textures.clear();
        this.batches[this._numBatches++] = batch;
        batch.topology = 'triangle-list';
        batch.blendMode = blendMode

        return batch;
    }

    private _finishBatch(instructionSet: InstructionSet)
    {
        const batch = this.batches[this._numBatches - 1];
        batch.action = this._action;

        batch.batcher = this as any as Batcher; // Interface of EffectBatcher must be equal to Batcher
        batch.start = this._batchIndexStart;
        batch.size = this._batchIndexSize - this._batchIndexStart;

        ++BATCH_TICK;

        instructionSet.add(batch);

        this._batchIndexStart = this._batchIndexSize;
    }

    public finish(instructionSet: InstructionSet)
    {
        this.break(instructionSet);
    }

    /**
     * Resizes the attribute buffer to the given size (1 = 1 float32)
     * @param size - the size in vertices to ensure (not bytes!)
     */
    public ensureAttributeBuffer(size: number)
    {
        if (size * 4 <= this.attributeBuffer.size) return;

        this._resizeAttributeBuffer(size * 4);
    }

    /**
     * Resizes the index buffer to the given size (1 = 1 float32)
     * @param size - the size in vertices to ensure (not bytes!)
     */
    public ensureIndexBuffer(size: number)
    {
        if (size <= this.indexBuffer.length) return;

        this._resizeIndexBuffer(size);
    }

    public destroy()
    {
        for (let i = 0; i < this._numBatches; i++)
        {
            returnBatchToPool(this.batches[i]);
        }

        this.batches = null as any as Batch[];

        this.indexBuffer = null as any as IndexBufferArray;

        this.attributeBuffer.destroy();
        this.attributeBuffer = null as any as ViewableBuffer;
    }

    private _resizeAttributeBuffer(sizeInBytes: number)
    {
        const newSizeInBytes = Math.max(sizeInBytes, this.attributeBuffer.size * 2);

        const newArrayBuffer = new ViewableBuffer(newSizeInBytes);

        fastCopy(this.attributeBuffer.rawBinaryData, newArrayBuffer.rawBinaryData);

        this.attributeBuffer = newArrayBuffer;
    }

    private _resizeIndexBuffer(size: number)
    {
        const indexBuffer = this.indexBuffer;
        const oldSize = indexBuffer.length;

        let newSize = Math.max(size, indexBuffer.length * 2);

        newSize += newSize % 2;

        // this, is technically not 100% accurate, as really we should
        // be checking the maximum value in the buffer. This approximation
        // does the trick though...

        // make sure buffer is always an even number..
        const newIndexBuffer = (newSize > 65535) ? new Uint32Array(newSize) : new Uint16Array(newSize);

        if (newIndexBuffer.BYTES_PER_ELEMENT !== indexBuffer.BYTES_PER_ELEMENT)
        {
            for (let i = 0; i < indexBuffer.length; i++)
            {
                newIndexBuffer[i] = indexBuffer[i];
            }
        }
        else
        {
            fastCopy(indexBuffer.buffer, newIndexBuffer.buffer);
        }

        const indexDisp = [0, 1, 2, 0, 2, 3];

        for (let index = oldSize; index < newSize; index ++)
        {
            const quadOffset = Math.floor(index / 6) * 4;
            newIndexBuffer[index] = quadOffset + indexDisp[index % 6];
        }

        this.indexBuffer = newIndexBuffer;
    }

    private _packAttributes(element: BatchableParticleElement, textureId: number)
    {
        const f32 = this.attributeBuffer.float32View;
        const u32 = this.attributeBuffer.uint32View;
        
        const textureIdAndRound = textureId << 16;

        const { quad } = element;

        let index = this.attributeSize;

        for (let i = 0; i < 4; i++)
        {
            const { position, color, texCoords } = quad[i];

            f32[index++] = position[0];
            f32[index++] = position[1];

            f32[index++] = texCoords[0];
            f32[index++] = texCoords[1];

            u32[index++] = color;
            u32[index++] = textureIdAndRound;
        }
    }
}

