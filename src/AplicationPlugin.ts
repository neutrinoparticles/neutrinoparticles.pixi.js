import { Application, ExtensionType, Renderer } from "pixi.js";
import { Context } from "./Context";

/**
 * Creates {@link Context} object
 * and assign it to PIXI.Application.neutrino property of the application.
 * 
 * @param {Object} options Options
 * @param {Object} options.neutrino Options object to pass to {@link Context} constructor.
 */
export class ApplicationPlugin
{
    static get extension(): any { return {
        name: 'neutrino',
        type: ExtensionType.Application,
    }}

    static init(this: Application, options: any): void
    {
        this.neutrino = new Context(this.renderer as Renderer, options.neutrino);
    }

    static destroy(this: Application): void
    {
        this.neutrino = undefined;
    }
}