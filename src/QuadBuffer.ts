import { Mat3x2, Vec2, Vec3, Vec4 } from "./types";
import { PerspectiveProjection } from "./PerspectiveProjection";

const _tempV3: Vec3 = [0, 0, 0];

export interface InputVertex {
  position: Vec3;
  color: Vec4;
  texCoords: Array<Vec2>;
};

export interface OutputVertex {
  position: Vec2;
  color: number;
  texCoords: Vec2;
};

export type OutputQuad = [OutputVertex, OutputVertex, OutputVertex, OutputVertex];

export type PushQuadCallback = (vertices: OutputQuad, renderStyle: number) => void;

const emptyOutputVertex = function(): OutputVertex {
  return {
    position: [0, 0],
    color: 0,
    texCoords: [0, 0]
  };
}

function rgbaToUint32(rgba: [number, number, number, number], alpha: number): number {
  const r = rgba[0];
  const g = rgba[1];
  const b = rgba[2];
  const a = Math.floor(rgba[3] * alpha);

  return (a << 24) | (b << 16) | (g << 8) | r;
}

export class QuadBuffer 
{
  public worldTransform: Mat3x2;
  public worldAlpha: number;

  private _pushQuadCallback: PushQuadCallback;
  private _vertices: OutputQuad;
  private _currentVertex: number;
  private _renderStyle: number;
  private _projection?: PerspectiveProjection;
  private _quadDiscarded: boolean;

  constructor(
    pushQuadCallback: PushQuadCallback, 
    projection?: PerspectiveProjection
  ) 
  {
    this.worldTransform = [0, 0, 0, 0, 0, 0];
    this.worldAlpha = 1.0;

    this._pushQuadCallback = pushQuadCallback;

    this._vertices = [ 
      emptyOutputVertex(),
      emptyOutputVertex(),
      emptyOutputVertex(),
      emptyOutputVertex()
    ];

    this._currentVertex = 0;
    this._renderStyle = 0;

    this._projection = projection;
    this._quadDiscarded = false;
  }

  initialize(_maxNumVertices: number, _texChannels: any, _indices: any, _maxNumRenderCalls: any) 
  {
  }

  beforeQuad(renderStyle: number) 
  {
    this._renderStyle = renderStyle;
    this._quadDiscarded = false;
  }

  pushVertex(vertex: InputVertex) 
  {
    const v = this._vertices[this._currentVertex];

    const wt = this.worldTransform;

    const x = vertex.position[0];
    const y = vertex.position[1];

    _tempV3[0] = wt[0] * x + wt[2] * y + wt[4];
    _tempV3[1] = wt[1] * x + wt[3] * y + wt[5];

    if (this._projection) {
      _tempV3[2] = vertex.position[2];

      this._quadDiscarded = this._quadDiscarded ||
        !this._projection.transformPosition(v.position, _tempV3);
    } else {
      v.position[0] = _tempV3[0];
      v.position[1] = _tempV3[1];
    }
    
    v.color = rgbaToUint32(vertex.color, this.worldAlpha);

    v.texCoords[0] = vertex.texCoords[0][0];
    v.texCoords[1] = 1.0 - vertex.texCoords[0][1];

    ++this._currentVertex;

    if (this._currentVertex == 4)
    {
      if (!this._quadDiscarded) {
        this._pushQuadCallback(this._vertices, this._renderStyle);
      }

      this._currentVertex = 0;
    }
  }

  pushRenderCall(_rc: any) 
  {
  }

  cleanup() 
  {
  }
}
