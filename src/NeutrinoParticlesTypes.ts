import { InputVertex } from "./QuadBuffer";
import { Vec2, Vec3, Vec4 } from "./types";

export interface NeutrinoEffectStartOptions
{
    generatorsPaused?: boolean;
    paused?: boolean;
}

export interface NeutrinoRenderStyle
{
    textureIndices: Array<number>;
    materialIndex: number;
}

export interface NeutrinoEffectModel
{
    textures: Array<string>;
    renderStyles: Array<NeutrinoRenderStyle>;
    materials: Array<number>;

    createWGLInstance: (position: Vec3 | null, rotation: Vec4 | null, renderBuffer: NeutrinoRenderBuffer,
        options?: NeutrinoEffectStartOptions) => NeutrinoEffect;
}

export interface NeutrinoEffect
{
    model: NeutrinoEffectModel;
    texturesRemap: Array<NeutrinoSubRect | null>;

    restart: (position?: Vec3, rotation?: Vec4, options?: NeutrinoEffectStartOptions) => void;
    update: (seconds: number, position?: Vec3, rotation?: Vec4) => void;
    resetPosition: (position?: Vec3, rotation?: Vec4) => void;
    setPropertyInAllEmitters: (propName: string, propValue: number | Vec2 | Vec3 | Vec4) => void;
    pauseAllEmitters: () => void;
    unpauseAllEmitters: () => void;
    areAllEmittersPaused: () => boolean;
    pauseGeneratorsInAllEmitters: () => void;
    unpauseGeneratorsInAllEmitters: () => void;
    areGeneratorsInAllEmittersPaused: () => boolean;
    getNumParticles: () => number;
    fillGeometryBuffers: (cameraRight: Vec3, cameraUp: Vec3, cameraDir: Vec3) => void;
}

export interface NeutrinoRenderBuffer
{
    initialize: (maxNumVertices: number, texChannels: any, indices: any, maxNumRenderCalls: any) => void;
    beforeQuad: (renderStyle: number) => void;
    pushVertex: (vertex: InputVertex) => void;
    pushRenderCall: (_rc: any) => void;
    cleanup: () => void;
}

export interface NeutrinoSubRect
{
    x: number;
    y: number;
    width: number;
    height: number;
}

export interface NeutrinoNoiseGenerator
{
    progress: number;
    step: () => boolean;
}

export interface NeutrinoContext
{
    SubRect: new (x: number, y: number, width: number, height: number) => NeutrinoSubRect;
    NoiseGenerator: new () => NeutrinoNoiseGenerator;

    axes2quat: (outQuat: Vec4, axisX: Vec3, axisY: Vec3, axisZ: Vec3) => void;
    axisangle2quat: (outQuat: Vec4, axis: Vec3, angle: number) => void;
    axisangle2quat_: (axis: Vec3, angle: number) => Vec4;
    initializeNoise: (path: string, successCallback: () => void, failCallback: () => void) => void;
}