import { DOMAdapter, ExtensionType, Loader, LoaderParserPriority, ResolvedAsset } from 'pixi.js';
import { EffectModel } from './EffectModel';
import { Context } from './Context';
import { AssetsTexturesLoader } from './TexturesLoader';

export const effectModelLoader = {
    extension: {
        type: ExtensionType.LoadParser,
        priority: LoaderParserPriority.Normal,
    },

    test(_url: string, loadAsset: ResolvedAsset, _loader: Loader)
    {
        const result = loadAsset.data && loadAsset.data.__neutrinoContext__
            && (loadAsset.data.__neutrinoContext__ instanceof Context);
        return result;
    },

    async load(url: string, _loadAsset: ResolvedAsset, _loader: Loader)
    {
        const response = await DOMAdapter.get().fetch(url);
        const sourceCode = await response.text();
        return sourceCode;
    },

    async testParse(this: Loader, _asset: any, options: ResolvedAsset) 
    {
        return options.data && options.data.__neutrinoContext__ 
            && (options.data.__neutrinoContext__ instanceof Context);
    },

    async parse(asset: string, options: ResolvedAsset, loader: Loader) 
    {
        return new Promise((resolve) => {
            const context = options.data.__neutrinoContext__;

            const effectModel = new EffectModel(context, asset, new AssetsTexturesLoader(loader));
            if (effectModel.ready()) {
                resolve(effectModel);
            } else {                
                effectModel.once('ready', () => {
                    resolve(effectModel);
                });
            }
        });
    },

    unload(_effectModel: EffectModel)
    {
        // destroy?
    }
};
