import { BLEND_MODES, ExtensionType, InstructionSet, RenderPipe, Texture, WebGLRenderer } from "pixi.js";
import { Effect } from "./Effect";
import { OutputQuad } from "./QuadBuffer";
import { BatchableParticleElement } from "./EffectBatcher";

const particleElement = new BatchableParticleElement();

export class EffectPipe implements RenderPipe<Effect>
{
    /** @ignore */
    public static extension = {
        type: [
            ExtensionType.WebGLPipes,
        ],
        name: 'neutrino',
    } as const;

    public readonly renderer: WebGLRenderer;
    private _instructionSet: InstructionSet = null as any as InstructionSet;
    
    constructor(renderer: WebGLRenderer)
    {
        this.renderer = renderer;
    }

    public validateRenderable(_renderable: Effect): boolean
    {
        // Returns true to rebuild all instruction set.
        return true;
    }

    // Added once - rendered many times
    public addRenderable(_effect: Effect, instructionSet: InstructionSet)
    {
        this._instructionSet = instructionSet;
        _effect._generateGeometry(this.renderer);
    }

    public updateRenderable(_effect: Effect)
    {
        // nothing to be done here!
    }

    public destroyRenderable(_effect: Effect)
    {
        //renderable.off('destroyed', this._destroyRenderableBound);
    }

    public execute(_effect: Effect): void
    {
    }

    public destroy(): void
    {
    }

    public batchQuad(quad: OutputQuad, texture: Texture, adjustedBlendMode: BLEND_MODES)
    {
        particleElement.texture = texture;
        particleElement.blendMode = adjustedBlendMode;
        particleElement.quad = quad;
        particleElement.instructionSet = this._instructionSet;

        this.renderer.renderPipes.batch.addToBatch(particleElement, this._instructionSet);
    }
}
