
/**
 * The class implements perspective projection transformation.
 * 
 * @example
 * // Create effect instance using loaded model
 * let effect = new PIXINeutrino.Effect(resources.effectModel, {
 *   position: [400, 300, 0],
 *   projection: new PIXINeutrino.PerspectiveProjection(60.0)
 * });
 * 
 * @param {number} [horizontalAngle] Horizontal angle of the perspective projection in degrees.
 */

import { Rectangle } from "pixi.js";
import { Vec2, Vec3 } from "./types";

export class PerspectiveProjection
{
    private _angleTan: number = 0;
    private _screenWidth: number = 0;
    private _screenPosX: number = 0;
    private _screenPosY: number = 0;
    private _z: number = 0;
    private _near: number = 0;

    set horizontalAngle(value: number) {
        this._angleTan = Math.tan((value * 0.5) / 180.0 * Math.PI);
    }

    constructor(horizontalAngle: number) {
        this.horizontalAngle = horizontalAngle;
    }

    /**
	 * Changes horizontal angle of the projection.
	 * 
	 * @param {number} value Angle in degrees.
	 */
    

    /**
     * Sets rendering frame for the projection.
     * 
     * This method shouldn't be called manually when the class is used with effects. It is called
     * on every render call for each effect automatically.
     * @param {PIXI.Rectangle} frame Rendering frame.
     */
    setScreenFrame(frame: Rectangle) {
        this._screenWidth = frame.width;
        this._screenPosX = frame.x + frame.width * 0.5;
        this._screenPosY = frame.y + frame.height * 0.5;
        this._z = this._screenWidth * 0.5 / this._angleTan;
        this._near = this._z * 0.99;
    }

    /**
     * Transforms 3D point accordingly to the projection.
     * 
     * Basically, point's position X and Y components are simply scaled dependently on Z position. The method
     * is used in WebGL and Canvas rendering.
     * 
     * @param {Array} out [x, y] Transformed position.
     * @param {Array} pos [x, y, z] Untransformed input vertex position.
     * @returns false, if a particle is on the back side of the camera and should be discarded. Otherwise - true.
     */
    transformPosition(out: Vec2, pos: Vec3): boolean {
        if (pos[2] > this._near) {
            return false;
        }

        const scale = this._getScale(pos);
        out[0] = (pos[0] - this._screenPosX) * scale + this._screenPosX;
        out[1] = (pos[1] - this._screenPosY) * scale + this._screenPosY;

        return true;
    }

    /**
     * Transforms 2D size of a particle accordingly to the projection.
     * 
     * Basically, size is simply scaled dependently on Z position of a particle. The method is used
     * only in Canvas rendering.
     * 
     * @param {Array} outSize [width, height] Transformed size.
     * @param {Array} pos [x, y, z] Untransformed particle position.
     * @param {Array} size [width, height] Untransformed size.
     */
    transformSize(outSize: Vec2, pos: Vec3, size: Vec2) {
        const scale = this._getScale(pos);
        outSize[0] = size[0] * scale;
        outSize[1] = size[1] * scale;
    }

    _getScale(pos: Vec3) {
        return this._z / (this._z - pos[2]);
    }
}