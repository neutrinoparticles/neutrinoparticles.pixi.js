/**
 * Enum for pause type. Used in {@link Effect} constructor.
 *
 * @property NO - Not paused. Effect will start immidiately. Passed 
 * starting position and rotation considered as global and will be used 
 * to start simulate effect. Please note, that this pause type is not suitable
 * for effects you want to attach to other containers.
 * @property BEFORE_UPDATE_OR_RENDER - Paused until the first update or render.
 * Effect will be paused until its first update or render call. This pause type
 * is useful in most of cases when you don't want to pause effect. 
 * It allows to place effect on the scene in several subsequent methods calls:
 * create effect, attach to some parent container and set position of
 * the effect in container.
 * @property YES - Effect paused until unpause() called.
 */

export enum Pause {
	NO,
	BEFORE_UPDATE_OR_RENDER,
	YES
}

export type Vec4 = [number, number, number, number];
export type Vec3 = [number, number, number];
export type Vec2 = [number, number];
export type Mat3x2 = [number, number, number, number, number, number];
