import { defineConfig } from 'vite';
import path from 'path';
import { exec } from 'child_process';

const postBuild = () => {
  exec('npm run postbuild', (err, stdout, stderr) => {
    console.log(`=== POST BUILD BEGIN ===`);
    if (err) {
      console.error(`Error: ${err.message}`);
      if (stdout) {
        console.error(`${stdout}`);
      }
      if (stderr) {
        console.error(`${stderr}`);
      }
	      
      return;
    }
    if (stderr) {
      console.error(`Stderr: ${stderr}`);
      return;
    }
    console.log(`${stdout}`);
    console.log(`=== POST BUILD END ===`);
  });
}

export default defineConfig({
  build: {
	sourcemap: true,
    lib: {
      entry: path.resolve(__dirname, 'src/index.ts'),
      name: 'PIXINeutrino',
      fileName: (format) => `neutrinoparticles.pixi.${format}.js`,
      formats: ['es', 'cjs', 'umd'] 
    },
    rollupOptions: {
      external: ['pixi.js'], 
      output: {
        globals: {
          'pixi.js': 'PIXI',
        }
      }
    },
  },
  plugins: [
    {
      name: 'postbuild',
      //handleHotUpdate: async () => {  },
      buildStart: async () => { postBuild(); },
    }
  ],
});
