function TestProjection3D(ctx)
{
    describe('Projection3D', function(cb) {

        it("Static", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/projection_3d.js', {
                    position: [400, 300, 0],
                    projection: new PIXINeutrino.PerspectiveProjection(60)
                })
            `);

            function runTest() {
                ibct.check();
                ibct.updateAndCheck(1);
                    
                ibct.finalize(cb);
            }
        });

        it("Scaled1", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/projection_3d_scale.js', {
                    position: [200, 200, 0],
                    scale: [2, 2, 2],
                    projection: new PIXINeutrino.PerspectiveProjection(90)
                })
            `);

            function runTest() {
                ibct.check();
                ibct.updateAndCheck(2);
                    
                ibct.finalize(cb);
            }
        });
        
        it("Scaled2", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/projection_3d_scale.js', {
                    position: [600, 400, 0],
                    scale: [2, 2, 2],
                    projection: new PIXINeutrino.PerspectiveProjection(90)
                })
            `);

            function runTest() {
                ibct.check();
                ibct.updateAndCheck(2);
                    
                ibct.finalize(cb);
            }
        });

    });
}

export {
    TestProjection3D
}