function TestTextureGrid(ctx)
{
    describe('TextureGrid', function(cb) {

        it("OutOfRange", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/texture_grid.js', {
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.check();
                ibct.updateAndCheck(1.8);
                    
                ibct.finalize(cb);
            }
        });
    });
}

export {
    TestTextureGrid
}