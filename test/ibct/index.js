import {IBCT} from './ibct.js';
import { TestAtlases } from './atlases.js';
import { TestBaseParent } from './base_parent.js';
import { TestPause } from './pause.js';
import { TestPositioning } from './positioning.js';
import { TestRotating } from './rotating.js';
import { TestScaling } from './scaling.js';
import { TestSceneHierarchy } from './scene_hierarchy.js';
import { TestSimulation } from './simulation.js';
import { TestUpdateRender } from './update_render.js';
import commandLineArgs from 'command-line-args';
import { TestBatching } from './batching.js';
import { TestTextureGrid } from './texture_grid.js';
import { TestProjection3D } from './projection_3d.js';
import { TestWorldAlpha } from './world_alpha.js';

const ctx = {
  ibct: null
}

describe('IBCT', function() {

  before(function() {

    const optionDefinitions = [
      { name: 'ibct-expect-dir', type: String, defaultOption: "__expect__" },
    ];

    const options = commandLineArgs(optionDefinitions, { partial: true });

    return new Promise((resolve) => {
      ctx.ibct = new IBCT({
         expectDir: options["ibct-expect-dir"] 
        }, resolve);
    });
  });

  after(function() {
    ctx.ibct.destroy();
    ctx.ibct = null;
  });

  function TestEverything(ctx)
  {
    TestAtlases(ctx);
    TestBaseParent(ctx);
    TestPause(ctx);
    TestPositioning(ctx);
    TestRotating(ctx);
    TestScaling(ctx);
    TestSceneHierarchy(ctx);
    TestSimulation(ctx);
    TestUpdateRender(ctx);
    TestTextureGrid(ctx);
    TestProjection3D(ctx);
    TestWorldAlpha(ctx);
  };

  describe('WebGL', function() {
    before(function() {
      ctx.ibct.setDefaultResetOptions({});
    });

    TestBatching(ctx);
    TestEverything(ctx);
  });

//  describe('Canvas', function() {
//    before(function() {
//      ctx.ibct.setDefaultResetOptions({forceCanvas: true});
//    });
//
//    TestEverything(ctx);
//  });

});
  
