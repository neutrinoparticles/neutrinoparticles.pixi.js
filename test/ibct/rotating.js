function toRadians(angle) {
    return Math.PI / 180 * angle;
}

function TestRotating(ctx)
{
    describe('Rotating', function(cb) {

        async function TestStaticRotating(angle, cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/rotating.js', { 
                    position: [400, 300, 0],
                    rotation: ` + toRadians(angle) + `
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        }

        // Static*
        {
            const angles = [ 45, 90, 135, 180, 225, 270, 315, -45, -90, -135, -180, -225, -270, -315 ];

            angles.forEach((angle) => {
                it("Static" + String(angle), function(cb) {
                    TestStaticRotating.call(this, angle, cb);    
                });
            });
        }

        async function TestRotatingByAngle(angle, cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/rotating.js', { 
                    position: [400, 300, 0],
                    rotation: 0
                })
            `);

            function runTest() {
                ibct.check();
                ibct.exec(`test.effect.rotation = rad(` + String(angle) + `);`);
                ibct.updateAndCheck(2);
                    
                ibct.finalize(cb);
            }
        }

        it("DynamicRight90", function(cb) {
            TestRotatingByAngle.call(this, 90, cb);
        });

        it("DynamicLeft90", function(cb) {
            TestRotatingByAngle.call(this, -90, cb);
        });

        it("Spiral", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/rotating.js', { 
                    position: [400, 300, 0],
                    rotation: 0
                })
            `);

            function runTest() {
                ibct.check();
                ibct.exec(`test.effect.rotation = rad(90);`);
                ibct.updateAndCheck(0.25);
                ibct.exec(`test.effect.rotation = rad(180);`);
                ibct.updateAndCheck(0.25);
                ibct.exec(`test.effect.rotation = rad(270);`);
                ibct.updateAndCheck(0.25);
                ibct.exec(`test.effect.rotation = rad(360);`);
                ibct.updateAndCheck(0.25);
                    
                ibct.finalize(cb);
            }
        });

        it("RotateResetRotate", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/rotating.js', { 
                    position: [400, 300, 0],
                    rotation: 0 
                })
            `);

            function runTest() {
                ibct.check();
                ibct.exec(`test.effect.rotation = rad(45);`);
                ibct.updateAndCheck(1);
                ibct.exec(`
                    test.effect.resetPosition(null, rad(90)); 
                    test.effect.rotation = rad(180);`);
                ibct.updateAndCheck(1);
                ibct.updateAndCheck(1);
                    
                ibct.finalize(cb);
            }
        });
    });
}

export {
    TestRotating
}