import { app, BrowserWindow, ipcMain } from 'electron';
import * as path from 'path';
const pixelmatch = require('pixelmatch');
import { PNG } from 'pngjs';
import * as fs from 'fs';
import assert from 'assert';

export class IBCT
{
    constructor(options, cb)
    {
        this._expectDir = options.expectDir || "__expect__";

        this._win = null;
        this._rendersInQueue_ = 0;
        this._errorDiff = false;
        this._errorMiss = false;
        this._errorInScript = null;
        this._finalizeCb = null;
        this._defaultResetOptions = null;
        this._createWindow(cb); 
        ipcMain.on('error', (obj, msg) => {
            this._errorInScript = msg;
        })
    }

    destroy()
    {
        if (this._win)
        {
            this._win.destroy();
            this._win = null;
        }
    }

    setDefaultResetOptions(options)
    {
        this._defaultResetOptions = options;
    }

    async reset(title, options, runTest)
    {
        this._title = title;
        this._counter = 0;
        this._errorDiff = false;
        this._errorMiss = false;
        this._errorInScript = null;

        ipcMain.once('effects-ready', runTest);

        options = Object.assign(this._defaultResetOptions || {}, options);

        options = Object.assign({
            autoStart: false,
            width: 800,
            height: 600,
            backgroundColor: 0x404040,
            neutrino: {}
        }, options);

        options.neutrino = Object.assign({
            texturesBasePath: path.join(__dirname, 'textures/')
        }, options.neutrino);

        return await this._win.webContents.executeJavaScript(
            `reset(${JSON.stringify(options)})`);
    }

    async update(dt)
    {
        return this.exec("test.update(" + String(dt) + ")");
    }

    check()
    {
        ++this._rendersInQueue_;
        return this.exec("return render()").then((dataUrl) => {
            --this._rendersInQueue_;

            const data = dataUrl.replace(/^data:image\/\w+;base64,/, "");
            
            const expectFilePath = path.join(__dirname, this._expectDir, this._filename());
            const resultFilePath = path.join(__dirname, '__result__', this._filename());
            const diffFilePath = path.join(__dirname, '__diff__', this._filename());

            const resultImage = PNG.sync.read(Buffer.from(data, 'base64'));

            if (fs.existsSync(expectFilePath))
            {
                const expectImage = PNG.sync.read(fs.readFileSync(expectFilePath));
                const {width, height} = expectImage;
                assert(resultImage.width === width && resultImage.height === height);
                const diffImage = new PNG({width, height});
                if (pixelmatch(expectImage.data, resultImage.data, diffImage.data, 
                    width, height, { threshold: 0 }))
                {
                    if (!fs.existsSync(path.dirname(diffFilePath)))
                        fs.mkdirSync(path.dirname(diffFilePath));

                    fs.writeFileSync(diffFilePath, PNG.sync.write(diffImage));

                    this._errorDiff = true;
                }
            }
            else
            {
                this._errorMiss = true;
            }

            if (!fs.existsSync(path.dirname(resultFilePath)))
                fs.mkdirSync(path.dirname(resultFilePath));

            fs.writeFileSync(resultFilePath, PNG.sync.write(resultImage));

            ++this._counter;

            this._tryFinalize();
        });
    }

    updateAndCheck(dt)
    {
        this.update(dt);
        return this.check();
    }

    finalize(cb)
    {
        this._finalizeCb = cb;
        this._tryFinalize();
    }

    _createWindow(cb)
    {
        this._win = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
                nodeIntegration: true,
                contextIsolation: false
            }
        });
        
        this._win.loadFile(path.join(__dirname, 'page.html')).then(cb);

        // Open the DevTools.
        //this._win.webContents.openDevTools();

        //this._win.on('closed', () => {
        //    this._win = null
        //});
    }

    exec(src)
    {
        const execPromise = this._win.webContents.executeJavaScript(
            `(function() {` + src + `})()`);
        
        const _this = this;
        execPromise.catch(function(reason) {
            _this._errorInScript = reason.message;
        })

        return execPromise;
    }

    _filename()
    {
        return this._title + ' ' + String(this._counter) + '.png';
    }

    _tryFinalize()
    {
        if (!this._finalizeCb)
            return;

        if (this._rendersInQueue_ == 0)
        {
            this.exec(``).then(function() {
                let errors = [];

                if (this._errorDiff)
                    errors.push("Differencies found!");

                if (this._errorMiss)
                    errors.push("Missed expectation file!");

                if (this._errorInScript)
                    errors.push(this._errorInScript);

                if (errors.length)
                    this._finalizeCb(errors);
                else
                    this._finalizeCb();

                this._finalizeCb = null;
            }.bind(this));
        }
    }
}