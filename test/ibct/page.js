const ipc =  require('electron').ipcRenderer;
const path = require('path');
const sinon = require('sinon');
const assert = require('assert');

window.onerror = (error) =>
{
  ipc.send('error', error);
}

let app, test = null;

let reset = async(options) =>
{
  test = {};

  if (app)
  {
    document.body.removeChild(app.canvas);
    PIXI.Assets.cache.reset();
    PIXI.Assets.loader.reset();
    PIXI.Assets.resolver.reset();
    PIXI.Cache.reset();

    app.destroy();
  }

  app = new PIXI.Application();
  await app.init(options);

  document.body.appendChild(app.canvas);
}

let loadEffect = async (path, options) =>
{
  PIXI.Assets.add({ alias: 'test_effect', src: path, data: app.neutrino.loadData });

  const resources = await PIXI.Assets.load(['test_effect']);

  test.effect = new PIXINeutrino.Effect(resources.test_effect,
    options);
  app.stage.addChild(test.effect);

  test.update = function(sec) 
  { 
    test.effect.update(sec); 
  }

  runTest();
}

let render = () =>
{
  app.render();
  return app.canvas.toDataURL();
}

let rad = (deg) =>
{
  return Math.PI / 180 * deg;
}

let runTest = () =>
{
  ipc.send('effects-ready');
}