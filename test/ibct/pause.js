function TestPause(ctx)
{
    describe('Pause', function(cb) {

        async function TestPause(cb, pause) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/pause.js', { 
                    position:[0, 0, 0],
                    pause: PIXINeutrino.Pause.` + pause + `
                })
            `);

            function runTest() {
                ibct.exec(`
                    let parent = new PIXI.Container();
                    parent.position = new PIXI.Point(400, 300);
                    app.stage.addChild(parent);
                    parent.addChild(test.effect);
                `);
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        }

        it("Default till update", function(cb) {
            TestPause.call(this, cb, 'BEFORE_UPDATE_OR_RENDER');
        });

        it("Yes", function(cb) {
            TestPause.call(this, cb, 'YES');
        });

        it("No", function(cb) {
            TestPause.call(this, cb, 'NO');
        });
    });

    describe('Pause generators', function(cb) {

        function TestPause(cb, generatorsPaused) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/pause.js', {
                    position: [0, 0, 0],
                    generatorsPaused: ` + generatorsPaused + `
                });
            `);

            function runTest() {
                ibct.exec(`
                    let parent = new PIXI.Container();
                    parent.position = new PIXI.Point(400, 300);
                    app.stage.addChild(parent);
                    parent.addChild(test.effect);
                `);
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        }

        it("Yes", function(cb) {
            TestPause.call(this, cb, 'true');
        });

        it("No", function(cb) {
            TestPause.call(this, cb, 'false');
        });
    });
}

export {
    TestPause
}