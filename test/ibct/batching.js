function TestBatching(ctx)
{
    
    describe('Batching', function(cb) {

        it("GeometryWithoutOverflow", function(cb) {
            const ibct = ctx.ibct;

            // Exact number of vertices used
            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.gl.useProgram = sinon.fake(
                    app.renderer.gl.useProgram);

                loadEffect('effects/batching/five_textures.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 1)
                    assert.equal(app.renderer.gl.useProgram.callCount, 1)
                `)
                ibct.finalize(cb);
            }
        });

        it("TexturesOverflow", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                PIXINeutrino.EffectBatcher.defaultOptions.maxTextures = 2;

                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.gl.useProgram = sinon.fake(
                    app.renderer.gl.useProgram);

                loadEffect('effects/batching/five_textures.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 3)
                    assert.equal(app.renderer.gl.useProgram.callCount, 1)
                    PIXINeutrino.EffectBatcher.defaultOptions.maxTextures = undefined;
                `)
                ibct.finalize(cb);
            }
        });

        it("SeveralEffects", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.gl.useProgram = sinon.fake(
                    app.renderer.gl.useProgram);

                PIXI.Assets.load({src: 'effects/batching/five_textures.js', data: app.neutrino.loadData})
                .then((effectModel) => 
                {
                    test.effect1 = new PIXINeutrino.Effect(
                        effectModel, {
                            position:[200, 150, 0]
                        });
                    app.stage.addChild(test.effect1);

                    test.effect2 = new PIXINeutrino.Effect(
                        effectModel, {
                            position:[600, 450, 0]
                        });
                    app.stage.addChild(test.effect2);

                    test.update = function(sec) 
                    { 
                        test.effect1.update(sec); 
                        test.effect2.update(sec); 
                    }
                    runTest();
                });
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 1)
                    assert.equal(app.renderer.gl.useProgram.callCount, 1)
                `)
                ibct.finalize(cb);
            }
        });

        it("SeveralEffectsDividedBySprite", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.gl.useProgram = sinon.fake(
                    app.renderer.gl.useProgram);

                PIXI.Assets.load({src: 'textures/test0.png'}).then(() => {
                    PIXI.Assets.load({src: 'effects/batching/five_textures.js', data: app.neutrino.loadData})
                    .then((effectModel) => 
                    {
                        test.effect1 = new PIXINeutrino.Effect(
                            effectModel, {
                                position:[200, 150, 0]
                            });
                        app.stage.addChild(test.effect1);

                        let sprite = PIXI.Sprite.from('textures/test0.png');
                        sprite.scale.set(3);
                        sprite.position.set(400, 300);
                        sprite.anchor.set(0.5);
                        app.stage.addChild(sprite);

                        test.effect2 = new PIXINeutrino.Effect(
                            effectModel, {
                                position:[600, 450, 0]
                            });
                        app.stage.addChild(test.effect2);

                        test.update = function(sec) 
                        { 
                            test.effect1.update(sec); 
                            test.effect2.update(sec); 
                        }
                        runTest();
                    });
                });
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 3)
                    assert.equal(app.renderer.gl.useProgram.callCount, 3)
                `)
                ibct.finalize(cb);
            }
        });

        it("SeveralEffectsDividedBy3Sprite", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.gl.useProgram = sinon.fake(
                    app.renderer.gl.useProgram);

                PIXI.Assets.load({src: 'textures/test0.png'}).then(() => {
                    PIXI.Assets.load({src: 'effects/batching/five_textures.js', data: app.neutrino.loadData})
                    .then((effectModel) => 
                    {
                        {
                            let sprite = PIXI.Sprite.from('textures/test0.png');
                            sprite.scale.set(3);
                            sprite.position.set(0, 0);
                            sprite.anchor.set(0.5);
                            app.stage.addChild(sprite);
                        }

                        test.effect1 = new PIXINeutrino.Effect(
                            effectModel, {
                                position:[200, 150, 0]
                            });
                        app.stage.addChild(test.effect1);

                        {
                            let sprite = PIXI.Sprite.from('textures/test0.png');
                            sprite.scale.set(3);
                            sprite.position.set(400, 300);
                            sprite.anchor.set(0.5);
                            app.stage.addChild(sprite);
                        }

                        test.effect2 = new PIXINeutrino.Effect(
                            effectModel, {
                                position:[600, 450, 0]
                            });
                        app.stage.addChild(test.effect2);

                        {
                            let sprite = PIXI.Sprite.from('textures/test0.png');
                            sprite.scale.set(3);
                            sprite.position.set(800, 600);
                            sprite.anchor.set(0.5);
                            app.stage.addChild(sprite);
                        }

                        test.update = function(sec) 
                        { 
                            test.effect1.update(sec); 
                            test.effect2.update(sec); 
                        }
                        runTest();
                    });
                });
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 5)
                    assert.equal(app.renderer.gl.useProgram.callCount, 5)
                `)
                ibct.finalize(cb);
            }
        });

        it("Blending", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.gl.useProgram = sinon.fake(
                    app.renderer.gl.useProgram);

                loadEffect('effects/batching/five_textures_blended.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 5)
                    assert.equal(app.renderer.gl.useProgram.callCount, 1)
                `)
                ibct.finalize(cb);
            }
        });
        
    });
}

export {
    TestBatching
}